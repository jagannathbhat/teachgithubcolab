(function () {
	let currentAction = 0
	let currentSlide = 0

	let nextAction = () => {
		if (currentAction < actions.length)
			actions[currentAction++]()
	}

	let nextSlide = () => {
		document.getElementById("slide" + currentSlide++).classList.add("slide--done")
		setTimeout(() => {
			document.body.style.transform = "translateY(-" + (currentSlide * 100) + "%)"
		}, 300)
	}

	let slide2action0 = () => {
		document.getElementById("slide2").classList.add("reveal0")
	}

	let slide3action0 = () => {
		document.getElementById("slide3").classList.add("reveal0")
	}

	let slide3action1 = () => {
		document.getElementById("slide3").classList.add("reveal1")
	}

	let slide3action2 = () => {
		document.getElementById("slide3").classList.add("reveal2")
	}

	let slide3action3 = () => {
		document.getElementById("slide3").classList.add("reveal3")
	}

	let slide3action4 = () => {
		document.getElementById("slide3").classList.add("reveal4")
	}

	let slide3action5 = () => {
		document.getElementById("slide3").classList.add("reveal5")
	}

	let actions = [
		nextSlide,
		nextSlide,
		slide2action0,
		nextSlide,
		slide3action0,
		slide3action1,
		slide3action2,
		slide3action3,
		slide3action4,
		slide3action5
	]

	document.addEventListener("keydown", event => {
		if (event.isComposing || event.keyCode === 229)
			return
		if (event.keyCode === 39 || event.keyCode === 34)
			nextAction()
	})
})()